# README для GitLab CI/CD конвейера

Этот конвейер автоматизирует сборку, публикацию и развертывание проекта Flask в различных окружениях (dev, staging и prod).

## Этапы конвейера

Конвейер состоит из следующих этапов:

1. **docker_build**: Сборка Docker образов и загрузка их в GitLab image registry.
2. **docker_publish**: Публикация собранных Docker образов в Docker Hub.
3. **to_dev**: Развертывание приложения в среде разработки с использованием Helm.
4. **to_staging**: Развертывание приложения в тестовой среде с использованием Helm.
5. **to_prod**: Развертывание приложения в продуктивной среде с использованием Helm.

## Включаемые файлы (Includes)

Конвейер включает в себя следующие файлы:

- `docker-build.yml` из проекта `va1erify-gitlab-ci/docker-build` (версия `v2.0.0`)
- `variables.yml` из проекта `va1erify-deusops/task-18-05-24/devops/variables/gitlabci-flow-app` (ветка `main`)
- `.helm.gitlab-ci.yml` из проекта `va1erify-gitlab-ci/helm` (версия `v2.0.0`)

## Правила (Rules)

Правила определяют, когда и при каких условиях должны выполняться задания:

- **docker_build_rules**: Правила для этапа сборки Docker.
- **main_rules**: Правила для остальных этапов конвейера, выполняются только при наличии тега.

## Скрипты (Scripts)

В конвейере используется общий скрипт `check-flow`, который выполняет следующие действия:

- Устанавливает теги и пути для Docker образов.
- Выводит на экран значения переменных `IMAGE_TAG` и `IMAGE_PATH`.

## Задания (Jobs)

Конвейер включает следующие задания:

1. **docker build and upload to gitlab image registry**: Сборка и загрузка Docker образов в GitLab image registry.
2. **release image to docker hub**: Публикация Docker образов в Docker Hub.
3. **helm install chart to dev**: Установка Helm чарта в dev окружении.
4. **helm install chart to staging**: Установка Helm чарта в staging окружении.
5. **helm install chart to prod**: Установка Helm чарта в prod окружении.


## Использование

Чтобы запустить этот конвейер, просто сделайте коммит и добавьте тег в репозиторий.
